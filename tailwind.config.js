/** @type {import('tailwindcss').Config} */
const daisyui = require('daisyui')
const daisyui_themes = require('daisyui/src/theming/themes')
const typography = require('@tailwindcss/typography')
const tailwind_theme = require('tailwindcss/defaultTheme')

function create_theme(theme_name, font_family) {
  return {
    [theme_name]: {
      ...daisyui_themes[theme_name],
      ...(font_family ? { fontFamily: font_family } : {}),
    },
  }
}

const my_themes = {
  ...create_theme('wireframe', 'Urbanist'),   // <-- Default Theme
  ...create_theme('black', 'Urbanist'),
  // create_theme('acid'),
  // create_theme('aqua'),
  // create_theme('autumn'),
  // create_theme('bumblebee'),
  // create_theme('business'),
  // create_theme('cmyk'),
  // create_theme('coffee'),
  // create_theme('corporate'),
  // create_theme('cupcake'),
  // create_theme('cyberpunk'),
  // create_theme('dark'),
  // create_theme('dracula'),
  // create_theme('emerald'),
  // create_theme('fantasy'),
  // create_theme('forest'),
  // create_theme('garden'),
  // create_theme('halloween'),
  // create_theme('lemonade'),
  // create_theme('light'),
  // create_theme('lofi'),
  // create_theme('luxury'),
  // create_theme('night'),
  // create_theme('pastel'),
  // create_theme('retro'),
  // create_theme('synthwave'),
  // create_theme('valentine'),
  // create_theme('winter'),
}

config = {
  content: ["./src/**/*.{html,md,njk}", "./.eleventy.js"],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Urbanist', ...tailwind_theme.fontFamily.sans],
      }
    }
  },

  daisyui: {
    themes: [ my_themes ],
    darkTheme: "black"
  },

  plugins: [typography, daisyui],
}

module.exports = config
