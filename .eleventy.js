const postcss = require('postcss');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

const metagen = require('eleventy-plugin-metagen');

module.exports = (eleventyConfig) => {
  eleventyConfig.addPlugin(metagen);

  eleventyConfig.addNunjucksAsyncFilter('postcss', (cssCode, done) => {
    postcss([tailwindcss(require('./tailwind.config.js')), autoprefixer(), cssnano()])
      .process(cssCode)
      .then(
        (r) => done(null, r.css),
        (e) => done(e, null)
      );
  });

  eleventyConfig.addPassthroughCopy('src/_redirects');

  eleventyConfig.addWatchTarget('src/assets/**/*');
  eleventyConfig.addPassthroughCopy('src/assets/webfonts/');

  return {
    dir: {
      input: 'src',
      output: 'public',
    },
  };
};
